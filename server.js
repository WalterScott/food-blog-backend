const path = require("path");
const express = require("express");
const mongoose = require("mongoose");

const bodyParser = require("body-parser");
const dotenv = require("dotenv");

const auth = require("./routes/auth");
const registration = require("./routes/registration");

const Article = require("./models/Article");

const app = express();
dotenv.config();

mongoose.connect(process.env.MONGODB_URL, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false
});

app.use(express.static(path.join("../food-blog/build")));
app.use(bodyParser.json());

// login
app.use("/api/auth", auth);

// registration
app.use("/api/registration", registration);

// create
app.post("/api/create-post", (req, res) => {
  const { credentials } = req.body;
  const newArticle = new Article({
    title: credentials.title,
    text: credentials.description
  });
  newArticle
    .save()
    .then(
      () => res.send({ status: "OK" }),
      () => res.status(400).json({ errors: { global: "Something goes wrong" } })
    );
});

// read
app.get("/api/articles", (req, res) => {
  Article.find().then(totalArticles => {
    Article.find()
      .skip(
        totalArticles.length -
          process.env.POST_COUNT_PER_PAGE * req.query.pageId >
          0
          ? totalArticles.length -
              process.env.POST_COUNT_PER_PAGE * req.query.pageId
          : 0
      )
      .limit(
        totalArticles.length -
          process.env.POST_COUNT_PER_PAGE * req.query.pageId >
          0
          ? process.env.POST_COUNT_PER_PAGE
          : totalArticles.length -
              process.env.POST_COUNT_PER_PAGE * (req.query.pageId - 1)
      )
      .then((articles, err) => {
        if (err) {
          res.status(400).json({ errors: { global: err } });
          return;
        }
        res.json({
          articles,
          pagesCount: Math.ceil(
            totalArticles.length / process.env.POST_COUNT_PER_PAGE
          )
        });
      });
  });
});

// read Article
app.get("/api/post/:id", (req, res) => {
  Article.findOne({ _id: req.params.id }).then((article, err) => {
    if (err) {
      res
        .status(400)
        .json({ errors: { global: "Sorry.. I can't find this article" } });
      return;
    }
    res.json({ post: article });
  });
});

// delete
app.delete("/api/delete-post/:id", (req, res) => {
  Article.deleteOne({ _id: req.params.id }).then(post => {
    if (post) res.json({ deletedPost: post });
    else {
      res
        .status(400)
        .json({ errors: { global: "This article is deleted yet" } });
    }
  });
});

// update
app.put("/api/update-post", (req, res) => {
  const { credentials } = req.body;
  Article.findByIdAndUpdate(
    credentials.id,
    {
      title: credentials.title,
      text: credentials.description
    },
    err => {
      if (err) {
        res.send(err);
        return;
      }
      res.send({ status: "updated" });
    }
  );
});

app.listen(process.env.PORT, () =>
  console.log("Listen on port:", process.env.PORT)
);
