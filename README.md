# Food blog Backend

1. git clone https://gitlab.com/WalterScott/food-blog-backend.git
   1.1 npm i
2. create .env file in food-blog-backend with:
   MONGODB_URL = mongodb://localhost/blog
   JWT_SECRET = secretkey
   PORT = 3020
   POST_COUNT_PER_PAGE = 6
3. start mongodb
   4.1 create db blog
   use blog
4. npm start from food-blog-backend
