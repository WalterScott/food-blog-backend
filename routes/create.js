function create(req, res) => {
    const { credentials } = req.body;
    const newArticle = new Article({
      title: credentials.title,
      text: credentials.description,
    });
    newArticle
      .save()
      .then(
        () => res.send({ status: 'OK' }),
        () => res.status(400).json({ errors: { global: 'Something goes wrong' } }),
      );
  };

  module.exports create;