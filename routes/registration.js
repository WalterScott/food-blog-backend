const express = require("express");
const User = require("../models/User");
const bcrypt = require("bcrypt");

const router = express.Router();

router.post("/", (req, res) => {
  const { credentials } = req.body;
  console.log(credentials);
  User.findOne({ email: credentials.email }).then(user => {
    if (user) {
      res.status(400).json({
        errors: {
          global: {
            text: "Email is occupied yet",
            id: "error.message.email.occupied"
          }
        }
      });
    } else {
      console.log(bcrypt.hashSync(credentials.password, 10));
      const newUser = new User({
        email: credentials.email,
        passwordHash: bcrypt.hashSync(credentials.password, 10)
      });
      newUser.save().then(
        () => res.send({ user: credentials.email }),
        () => {
          res.status(400).json({
            errors: {
              global: {
                text: "Something goes wrong",
                id: "error.message.something.wrong"
              }
            }
          });
        }
      );
    }
  });
});

module.exports = router;
